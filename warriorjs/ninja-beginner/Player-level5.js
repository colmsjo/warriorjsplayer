var _health = 20;

class Player {
  playTurn(warrior) {
    if (warrior.feel().isCaptive())
      warrior.rescue();
    else if (warrior.health() < _health) {
      if ( warrior.feel().isEmpty() ) {
        warrior.walk();
      } else {
        warrior.attack();
      }
    } else {
      if (warrior.health() == 20) {
        if ( warrior.feel().isEmpty() ) {
          warrior.walk();
        } else {
          warrior.attack();
        }
      } else {
        warrior.rest();
      }
    }

    _health = warrior.health();
  }
}

global.Player = Player;

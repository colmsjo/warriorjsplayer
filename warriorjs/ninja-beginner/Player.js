var _health = 20;
var _underAttack = false;
var _captiveRescued = false;

var move = function (warrior, direction) {
    if (warrior.feel(direction).isCaptive()) {
      warrior.rescue(direction);
      _captiveRescued = true;
    }
    else if (_underAttack) {
      if ( warrior.feel(direction).isEmpty() ) {
        warrior.walk(direction);
      } else {
        warrior.attack(direction);
      }
    } else {
      if (warrior.health() == 20) {
        if ( warrior.feel().isEmpty() ) {
          warrior.walk(direction);
        } else {
          warrior.attack(direction);
        }
      } else {
        warrior.rest();
      }
    }
}

class Player {
  playTurn(warrior) {
    _underAttack = warrior.health() < _health;

    if (!_captiveRescued)
      move(warrior, 'backward');
    else {
      if (warrior.health() < 10)
        move(warrior, 'backward');
      else
        move(warrior, 'forward');
    }

    _health = warrior.health();
  }
}

global.Player = Player;
